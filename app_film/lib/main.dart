import 'dart:convert';

import 'package:app_film/class/tv_show.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Tv Show App',
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentPage = 1;

  void _increment() {
    setState(() {
      _currentPage++;
    });
  }

  void _resetIncrement() {
    setState(() {
      _currentPage = 1;
    });
  }

  Future<List<TvShow>> fetchTvShows(int currentPage) async {
    final response = await http.get(Uri.parse(
        'https://www.episodate.com/api/most-popular?page=$currentPage'));
    if (response.statusCode == 200) {
      final jsonResponse = jsonDecode(response.body);
      List<dynamic> tvShowsJson = jsonResponse['tv_shows'];
      return tvShowsJson.map((json) => TvShow.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load TV shows');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF01031C),
      appBar: AppBar(
        backgroundColor: const Color(0xFF202238),
        title: const Text(
          "TV Show App",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            onPressed: _resetIncrement,
            icon: const Icon(Icons.update, color: Colors.white),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _increment,
        backgroundColor: Colors.deepOrange,
        tooltip: 'Increment',
        child: const Icon(Icons.add, color: Colors.white),
      ),
      body: Center(
        child: FutureBuilder<List<TvShow>>(
          future: fetchTvShows(_currentPage),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
              return const Center(child: Text('No TV shows found'));
            } else {
              return Padding(
                padding: const EdgeInsets.all(10),
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 10.0),
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    final tvShow = snapshot.data![index];
                    return Card(
                      color: const Color(0xFF202238),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Expanded(
                              child: Image.network(
                                tvShow.imageThumbnailPath,
                                fit: BoxFit.cover,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                tvShow.name,
                                style: const TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
